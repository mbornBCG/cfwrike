# Wrike API SDK for Coldfusion

Interact with the Wrike API from Coldfusion.

## Getting Started

1. Get an [API Token from Wrike][1]
2. Clone this repo. `git clone git@bitbucket.org:mbornBCG/cfwrike.git`
3. Rename `config-sample.cfm` as `config.cfm`. `mv config-sample.cfm config.cfm`
4. Place your Wrike API Token in the config file. `vim config.cfm`
5. Start Commandbox and run the server to view demos. `box server start`.

## Layout

* All api integration components are in `api/`
* See API examples in the `demos/` folder

## TODO

Apologies, this is the bare bones of an API integration. There's a lot of work still to do. PR's welcome!

* Many unsupported API endpoints
* Of the supported endpoints, many parameters are still unsupported.
* Ugly mix of script and tag syntax in `apis/` folder

[1]:https://developers.wrike.com/getting-started/