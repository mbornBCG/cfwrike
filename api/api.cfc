component hint="Provide useful api-like utility functions to wrap the API in." {

  /**
   * an HTTP wrapper for a GET call.
   */
  package struct function _get(required string url, struct params) {
    cfhttp( url="#application.wrikeURI#/#arguments.url#", result="local.cfhttp", method="GET" ) {

      // assuming we need authorization for EVERY call...
      cfhttpparam( name="Authorization", type="Header", value="Bearer #application.wrikeToken#" );

      // supports dynamic parameters, like _get("google.com",{q='how to api call'})
      for (local.arg in arguments.params) {
        cfhttpparam( name=local.arg, type="url", value = Serialize(arguments.params[local.arg]) );
      }
    }
    return handleResult(local.cfhttp);
  }

  /**
   * given an HTTP response, do stuff with status codes and errors and such.
   */
	public struct function handleResult(required struct http ) {
		if (arguments.http.statuscode > 299) {
			saveError(arguments.http);
		}
		
		return DeSerializeJSON(arguments.http.filecontent);
	}
	
	public void function saveError(required struct http) {
		this.latestError = arguments.http;
	}
	
	public void function getError(required struct http) {
		this.latestError = arguments.http;
	}
}
