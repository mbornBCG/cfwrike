<cfcomponent bindingname="wrikeAccount" hint="Integrate with Wrike Accounts API." extends="api">
	<cfset this.name = "wrikeAccount" />
	
	<!---
		TODO: CREATE GROUP
		
		@cite https://developers.wrike.com/documentation/api/methods/create-groups
	--->
	<cffunction name="new" returnType="boolean" access="public" output="false">
		<cfreturn FALSE />
	</cffunction>
	
	<!---
		TODO: MODIFY GROUPS
		
		@cite https://developers.wrike.com/documentation/api/methods/modify-groups
	--->
	<cffunction name="update" returnType="boolean" access="public" output="false">
		<cfreturn FALSE />
	</cffunction>
	
	<!---
		TODO: DELETE GROUPS
		
		@cite https://developers.wrike.com/documentation/api/methods/delete-groups
	--->
	<cffunction name="delete" returnType="boolean" access="public" output="false">
		<cfreturn FALSE />
	</cffunction>
	
	<!---
		GET SINGLE GROUP
		/groups/{groupid}
		
		@cite https://developers.wrike.com/documentation/api/methods/query-groups
	--->
	<cffunction name="getSingle" returnType="struct" access="public" output="false">
		<cfargument name="groupid" required="true" type="string" hint="Wrike Group ID." />
		
		<cfhttp method="GET" url="#application.wrikeURI#/groups/#arguments.groupid#" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization"  value="Bearer #application.wrikeToken#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
	
	<!---
		LIST ALL GROUPS IN AN ACCOUNT
		/accounts
		
		@cite https://developers.wrike.com/documentation/api/methods/query-groups
	--->
	<cffunction name="getByAccount" returnType="struct" access="public" output="false">
		<cfargument name="accountid" required="true" type="string" hint="Wrike Account ID." />
		
		<cfhttp method="GET" url="#application.wrikeURI#/accounts/#arguments.accountid#/groups" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization"  value="Bearer #application.wrikeToken#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
</cfcomponent>