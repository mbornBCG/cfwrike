/**
 * Integrate with Wrike Timelog API.
 */
component bindingname="wrikeTimelog" hint="Integrate with Wrike Timelog API." extends="api" {
  /* 
    LIST TIME LOGS
    @cite https://developers.wrike.com/documentation/api/methods/query-timelogs
  */

  public struct function get(
    struct createdDate = {},
    struct trackedDate = {},
    boolean me = false,
    boolean descendants = true,
    boolean subTasks = true,
    boolean plainText = false,
    array timelogCategories = []
  ) output=true {
    local.params = arguments;
    
    // certain complex arguments should not be blank / empty structs
    structDelete(local.params,"createdDate");
    structDelete(local.params,"trackedDate");
    
    return _get(
      url = "/timelogs",
      params = local.params
    );
  }
  public struct function getByFolder(
    required string folderid
  ) output=false {
    cfhttp( url="#application.wrikeURI#/folders/#arguments.folderid#/timelogs", result="local.cfhttp", method="GET" ) {
      cfhttpparam( name="Authorization", type="Header", value="Bearer #application.wrikeToken#" );
    }
    return handleResult(local.cfhttp);
  }
}
