<cfcomponent bindingname="wrikeAccount" hint="Integrate with Wrike Accounts API." extends="api">
	<cfset this.name = "wrikeAccount" />
	
	<!---
		LIST ACCOUNTS OR GET SINGLE ACCOUNT
		/accounts
		/accounts/{accountid}
		
		@cite https://developers.wrike.com/documentation/api/methods/query-accounts
	--->
	<cffunction name="get" returnType="struct" access="public" output="false">
		<cfargument name="accountid" required="false" type="string" default="" hint="Wrike Account ID." />
		
		<cfif arguments.accountid GT "">
			<cfset local.response = this.getSingle(arguments.accountid)>
		<cfelse>
			<cfset local.response = this.getAll()>
		</cfif>
		
		<cfreturn local.response />
	</cffunction>
	
	<!---
		GET SINGLE ACCOUNT
		/accounts/{accountid}
		
		@cite https://developers.wrike.com/documentation/api/methods/query-accounts
	--->
	<cffunction name="getSingle" returnType="struct" access="private" output="false">
		<cfargument name="accountid" required="true" type="string" hint="Wrike User ID." />
		
		<cfhttp method="GET" url="#application.wrikeURI#/accounts/#arguments.accountid#" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization"  value="Bearer #application.wrikeToken#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
	
	<!---
		LIST ALL ACCOUNTS
		/accounts
		
		@cite https://developers.wrike.com/documentation/api/methods/query-accounts
	--->
	<cffunction name="getAll" returnType="struct" access="private" output="false">
		
		<cfhttp method="GET" url="#application.wrikeURI#/accounts" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization"  value="Bearer #application.wrikeToken#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
</cfcomponent>