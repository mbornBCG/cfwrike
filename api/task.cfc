<cfcomponent bindingname="wrikeTask" hint="Integrate with Wrike Tasks API." extends="api">
	<cfset this.name = "wrikeTask" />
	
	<!---
		CREATE TASK
		@cite https://developers.wrike.com/documentation/api/methods/create-task
	--->
	<cffunction name="new" access="public" returnType="struct" output="false">
		<cfargument name="folderid" required="true" />
		<cfargument name="responsibles" required="false" type="array" default="#[]#" hint="Assign this task to [X,Y] USER IDs." />
		<cfargument name="shareds" required="false" type="array" default="#[]#" hint="Share this task with [X,Y] USER IDs." />
		<cfargument name="importance" required="false" type="string" />
		<cfargument name="status" required="false" type="string" />
		<cfargument name="title" required="false" type="string" />
		
		<cfhttp method="POST" url="#application.wrikeURI#/folders/#arguments.folderid#/tasks" result="wrikeResults">
			<cfhttpparam type="Header" name="Authorization"  value="Bearer #application.wrikeToken#" />
			<cfhttpparam type="formfield" name="title" value="#arguments.title#" />
			<cfhttpparam type="formfield" name="status" value="#arguments.status#" />
			<cfhttpparam type="formfield" name="importance" value="#arguments.importance#" />
			<cfhttpparam type="formfield" name="dates" value='NOT_WORKING' />
			<cfhttpparam type="formfield" name="responsibles" value='#SerializeJSON(arguments.responsibles)#' />
			<cfhttpparam type="formfield" name="shareds" value='#SerializeJSON(arguments.shareds)#' />
			<cfhttpparam type="formfield" name="description" value="#simpleOutput#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
</cfcomponent>