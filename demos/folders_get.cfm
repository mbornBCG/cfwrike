<div class="section">
	<div class="container">
		<h2 class="title">Get All Folders</h2>
		
		<cfset wrikeFolders = CreateObject("api.folder") />
		<cfset allFolders = wrikeFolders.get() />
		
		<cfdump var="#allFolders#" />
	</div>
</div>
<div class="section">
	<div class="container">
		<h2 class="title">Get Folders By Account</h2>
		
		<cfset allFolders = wrikeFolders.getByAccount(
			accountid = "account_id_here"
		) />
		
		<cfdump var="#allFolders#" />
	</div>
</div>